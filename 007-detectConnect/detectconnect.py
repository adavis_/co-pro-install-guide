import subprocess
from twisted.internet import task
from twisted.internet import reactor

#how frequent to check for new connections
timeout = 15 #15 seconds

oldIpList = []

def getIpConnect():
    #get the ip addesses which have connected
    p1 = subprocess.Popen(["ip", "neighbor"], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["grep", "REACHABLE"], stdin=p1.stdout, stdout=subprocess.PIPE)
    p3 = subprocess.Popen(["grep", "192.168.2.[1-254]"], stdin=p2.stdout, stdout=subprocess.PIPE)
    p4 = subprocess.Popen(["cut", "-f", "1", "-d", " "], stdin=p3.stdout, stdout=subprocess.PIPE)

    global oldIpList
    ipList = filter(None, p4.communicate()[0].split('\n'))

    #empty list on startup, we dont want to alert
    if not oldIpList:
        oldIpList = ipList
        return
    else:
        if set(oldIpList) is not set(ipList):
            if len(oldIpList) < len(ipList): #new connection
                print "New Network Connection from ip " + ", ".join(list(set(ipList) - set(oldIpList)))
                oldIpList = ipList
            elif len(oldIpList) > len(ipList):
                print "Device " + ", ".join(list(set(oldIpList) - set(ipList))) + " removed from network"
                oldIpList = ipList
            else:
                pass

loop = task.LoopingCall(getIpConnect)
loop.start(timeout)

reactor.run()

